<?php
return [
    'BOng TOch'=>'បងតូច',
    'Home'=>'ទំព័រដើម',
    'Contant'=>'មាតិកា',
    'Search'=>'ស្វែងរក',
    'Starter Pages'=>'ទំព័រចាប់ផ្ដើម',
    'Active Page'=>'ទំព័រសកម្ម',
    'Inactive Page'=>'ទំព័រអសកម្ម',
    'Simple Link'=>'តំណសាមញ្ញ',
    'Call me whenever you can...'=>'ហៅទូរស័ព្ទមកខ្ញុំនៅពេលណាដែលអ្នកអាច ...',
    'Hours Ago'=>'ម៉ោង​កន្លងទៅ',
    'See All Messages'=>'មើលសារទាំងអស់',
    'New'=>'ថ្មីៗ'
];
