@extends('layouts.master')
@section('title', 'Orders')
@section('content','List orders')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Lists of orders</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <th>Id</th>
                            <th>Order ID</th>
                            <th>Product ID</th>
                            <th>QTY</th>
                            <th>Price</th>
                            <th>Descount</th>
                            <th>Action</th>
                        </tr>

                        @if (count($orderdetails)>0)
                        @foreach ($orderdetails as $orderdetail)
                        <tr>
                            <td>{{  $orderdetail->id }}</td>
                            <td>{{ $orderdetail->order_id }}</td>
                            <td>{{ $orderdetail->product_id }} </td>
                            <td>{{ $orderdetail->qty }} </td>
                            <td>{{ $orderdetail->price }} </td>
                            <td>{{ $orderdetal->descount }}</td>
                            <td>
                                <div class="btn-group show">
                                    <a href="" class=" btn btn-info">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="" class=" btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger btn-delete" href="">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else()
                        <tr>
                            <td colspan="7">
                                <h2 class="text-center">No Data</h2>
                            </td>
                        </tr>

                        @endif

                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-fooler">
                {{ $orderdetails->links() }}
            </div>
        </div>

    </div>
</div>
{{-- @push('script')
<script>
    $(document).ready(function () {
        $('.btn-delete').click(function () {
            console.log("hello");
            let id = $(this).data('id');
            let csrf = $('meta[name="csrf-token"]').attr('content');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('orders.destroy', $order->id) }}",
method: "POST",
data: {
'id': id,
'_method': 'DELETE',
'_token': csrf
},
success: function () {
console.log('success');

},
error: function () {
console.log('error');

}
})
}
})

})

})
</script>
@endpush --}}
@endsection
