@extends('layouts.master')
@section('title', 'Orders')
@section('content','Create orders')
@section('big-content','home')

@section('header-content')
<form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-6" style="margin:auto; margin-top:50px">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">Please input your information</h3>
                </div>
                <div class="card-body">
                    @if ($errors->Any())
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                        @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-6">
                            Name:<input type="text" class="form-control" placeholder="Enter name" name="name">
                        </div>
                        <div class="col-md-6">
                            Number phone:<input type="text" class="form-control" placeholder="Enter phone" name="tel">
                        </div>

                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            Date:<input type="text" class="form-control" placeholder="Enter date" name="date">
                        </div>
                        <div class="col-md-6">
                            Ship via:<input type="text" class="form-control" placeholder="Enter ship" name="ship_via">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-6">
                            Ship price:<input type="text" class="form-control" placeholder="Enter ship price"
                                name="ship_price">
                        </div>
                        <div class="col-md-6">
                            Total:<input type="text" class="form-control" placeholder="Enter total" name="total">
                        </div>
                    </div>

                    <button type="button" class="btn btn-danger">Cencel</button>
                    <button type="Submit" class="btn btn-success float-right">Create</button>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>


</form>

@endsection
