@extends('layouts.master')
@section('title', 'show')
@section('content','Show Order')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-6" style="margin:auto; margin-top:50px">
        <!-- Box Comment -->
        <div class="card card-widget bg-info text-white">
            <div class="card-header">
                <div class="user-block">
                    <h2>Product description</h2>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                        <i class="fa fa-circle-o"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <div class="row ">
                <div class="col-md-12">
                    <div class="card-body">
                        <h6>ID: {{ $order->id }}</h6>
                        <h6>Name: {{ $order->name }}</h6>
                        <h6>Phone: {{ $order->tel }}</h6>
                        <h6>Date: {{ $order->date }}</h6>
                        <h6>Ship via: {{ $order->ship_via }}</h6>
                        <h6>Ship price: {{ $order->ship_price }}</h6>
                        <h6>Total: {{ $order->total }}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
