<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/adminlte.min.js') }}"></script>
<script src="{{ asset('/js/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/sweetalert/sweetalert.all.js') }}"></script>


@if (Session::has('alert.config'))

<script>
    swal.fire({
        !!Session::pull('alert.config') !!
    })

</script>

@endif
@stack('script')
