<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @include('layouts.style')

</head>

<body class="hold-transition sidebar-mini">

    @include('layouts.navbar')
    @include('layouts.leftsidebar')
    @include('layouts.control-sidebar')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">@yield('content')</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">@yield('big-content')</a></li>
                            <li class="breadcrumb-item active">@yield('content')</li>
                        </ol>

                    </div>
                </div>

                @yield('header-content')

            </div>
        </div>
    </div>

    @include('layouts.footer')

    @include('layouts.script')

    @include('sweetalert::alert')

</body>

</html>
