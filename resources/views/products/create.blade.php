@extends('layouts.master')
@section('title', 'Ptoducts')
@section('content','Create products')
@section('big-content','home')

@section('header-content')
<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-8">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">Please input your information</h3>
                </div>
                <div class="card-body">
                    @if ($errors->Any())
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                        @foreach ($errors->all() as $error)
                        <p>{{ $error }}</p>
                        @endforeach
                    </div>
                    @endif
                    <div class="row form-group">
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter name" name="name">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter code" name="code">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter supplier" name="supplier_name">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-4">
                            <select class="form-control" name="category_id">
                                @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter price" name="price">
                        </div>
                        <div class="col-md-4">
                            <input type="text" class="form-control" placeholder="Enter QTY" name="onhand">
                        </div>
                    </div>

                    <button type="button" class="btn btn-danger">Cencel</button>
                    <button type="Submit" class="btn btn-success float-right">Sunmit</button>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">Please Preview</h3>
                </div>
                <div class="card-body">
                    <div class="row form-group">
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="input-img" name="imageurl">
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 mt-2 ">
                        <img id="preview" class="img-fluid" src="{{ asset('/img/placeholder.png') }}" alt="placeholder">
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</form>

@push('script')
<script>
    $(document).ready(function () {
        $('#input-img').change(function () {
            let img = $('#input-img')[0].files[0]
            let preview = $('#preview')
            let reader = new FileReader;
            if (img) {
                reader.readAsDataURL(img)
            }

            reader.onload = function (e) {
                preview.attr('src', e.target.result)
            }

        })

    })

</script>
@endpush

@endsection
