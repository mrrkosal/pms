@extends('layouts.master')
@section('title', 'show')
@section('content','Show productd')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-12">
        <!-- Box Comment -->
        <div class="card card-widget bg-info text-white">
            <div class="card-header">
                <div class="user-block">
                    <h2>Product description</h2>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                        <i class="fa fa-circle-o"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <div class="row ">
                <div class="col-md-6">
                    <div class="card-body">
                        <img class="img-fluid pad" src="{{ asset('storage'.'/'.$product->imageurl) }}" alt="Photo">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card-body" style="padding-top:60px">
                        <h6>ID: {{ $product->id }}</h6>
                        <h6>Product name: {{ $product->name }}</h6>
                        <h6>Code: {{ $product->code }}</h6>
                        <h6>Price: {{ $product->price }}</h6>
                        <h6>Onhand: {{ $product->onhand }}</h6>
                        <h6>Supplier: {{ $product->supplier_name }}</h6>
                        <h6>Category ID: {{ $product->category_id }}</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
