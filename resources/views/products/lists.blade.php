@extends('layouts.master')
@section('title', 'Products')
@section('content','Lists Products')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Lists of product</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Price</th>
                            <th>Onhand</th>
                            <th>Supplier</th>
                            <th>Category</th>
                            <th style="width:150px">Image</th>
                            <th style="width: 40px">Action</th>
                        </tr>

                        @if (count($products)>0)
                        @foreach ($products as $product)
                        <tr>
                            <td>{{  $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->code }} </td>
                            <td>{{ $product->price }} </td>
                            <td>{{ $product->onhand }} </td>
                            <td>{{ $product->supplier_name }} </td>
                            <td>{{ $product->category_id }} </td>
                            <td>
                                <img class="img-fluid" src="{{ asset('storage'.'/'.$product->imageurl) }}" alt="">
                            </td>
                            <td>
                                <div class="btn-group show">
                                    <a href="{{ route('products.show', $product->id) }}" class="btn btn-info">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a class="btn btn-danger btn-delete" data-id="{{ $product->id }}">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else()
                        <tr>
                            <td colspan="4">
                                <h2 class="text-center">No Data</h2>
                            </td>
                        </tr>

                        @endif

                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-fooler">
                {{ $products->links() }}
            </div>
        </div>

    </div>
</div>
@push('script')
<script>
    $(document).ready(function () {
        $('.btn-delete').click(function () {
            console.log("hello");
            let id = $(this).data('id');
            console.log(id);

            let csrf = $('meta[name="csrf-token"]').attr('content');
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
S
                    $.ajax({
                        url: "{{ route('products.destroy', $product->id) }}",
                        method: "POST",
                        data: {
                            'id': id,
                            '_method': 'DELETE',
                            '_token': csrf
                        },
                        success: function () {
                            console.log('success');

                        },
                        error: function () {
                            console.log('error');

                        }
                    })
                }
            })

        })

    })

</script>
@endpush
@endsection
