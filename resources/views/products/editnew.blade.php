@extends('layouts.master')
@section('title', 'Update')
@section('content','Update products')
@section('big-content','home')

@section('header-content')
<form method="POST" action="{{ route('products.update', $product->id) }}" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div class="col-md-12">
        <!-- Box Comment -->
        <div class="card card-widget">
            <div class="card-header">
                <div class="user-block">
                    <h6 class="description">Products preview</h6>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                        <i class="fa fa-circle-o"></i></button>
                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
                <!-- /.card-tools -->
            </div>
            <div class="row">
                <div class="card-body col-md-6">
                    <img id="preview" class="img-fluid pad" src="{{ asset('storage'.'/'.$product->imageurl) }}"
                        alt="Photo">

                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="input-img" name="imageurl">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                    </div>

                </div>
                <div class="col-md-6" style="padding:30px">
                    <div class="card-comment">
                        <div class="row form-group">
                            <div class="col-md-6">
                                Name: <input type="text" class="form-control" placeholder="Enter name" name="name"
                                    value="{{ $product->name }}">
                            </div>
                            <div class="col-md-6">
                                Code:<input type="text" class="form-control" placeholder="Enter code" name="code"
                                    value="{{ $product->code }}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                Price:<input type="text" class="form-control" placeholder="Enter price" name="price"
                                    value="{{ $product->price }}">
                            </div>
                            <div class="col-md-6">
                                supplier:<input type="text" class="form-control" placeholder="Enter supplier"
                                    name="supplier_name" value="{{ $product->supplier_name }}">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-6">
                                Category:<select class="form-control" name="category_id">
                                    @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6">
                                QTY:<input type="text" class="form-control" placeholder="Enter onhand" name="onhand"
                                    value="{{ $product->onhand }}">
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-danger">Cencel</button>
                    <button type="Submit" class="btn btn-success float-right">Update</button>
                </div>
            </div>
        </div>
    </div>
</form>

@push('script')
<script>
    $(document).ready(function () {
        $('#input-img').change(function () {
            let img = $('#input-img')[0].files[0]
            let preview = $('#preview')
            let reader = new FileReader;
            if (img) {
                reader.readAsDataURL(img)
            }

            reader.onload = function (e) {
                preview.attr('src', e.target.result)
            }

        })

    })

</script>
@endpush
@endsection
