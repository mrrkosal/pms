@extends('layouts.master')
@section('title', 'category')
@section('content','Lists category')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-12">

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Lists of product</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body p-0">
                <table class="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th style="width: 40px">Action</th>
                        </tr>

                        @if (count($categories)>0)
                        @foreach ($categories as $category)
                        <tr>
                            <td>{{  $category->id }}</td>
                            <td>{{ $category->name }}</td>
                            <td>{{ $category->description }} </td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{ route('categories.show', $category->id) }}" class="btn btn-info ">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">
                                        <i class="fa fa-edit"></i>
                                    </a>
                                    <a data-id='{{ $category->id }}' class="btn btn-danger btn-delete">
                                        <i class="fa fa-trash"></i>
                                    </a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else()
                        <tr>
                            <td colspan="4">
                                <h2 class="text-center">No Data</h2>
                            </td>
                        </tr>

                        @endif

                    </tbody>
                </table>

            </div>
            <!-- /.card-body -->
            <div class="card-fooler">
                {{ $categories->links() }}
            </div>
        </div>

    </div>
</div>
@push('script')
@if (count($categories)>0)
<script>
    $(document).ready(function () {
        $('.btn-delete').click(function () {
            var id = $(this).data('id');
            var csrf = $('meta[name="csrf-token"]').attr('content');

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: "{{ route('categories.destroy', $category->id) }}",
                        method: "POST",
                        data: {
                            'id': id,
                            '_method': 'DELETE',
                            '_token': csrf
                        },
                        success: function () {
                            Swal.fire(
                                'Deleted!',
                                'Your file has been deleted.',
                                'success'
                            )
                        },
                        error: function () {
                            console.log('Error');

                        }
                    })
                }
            })
        })
    })

</script>
@endif
@endpush
@endsection
