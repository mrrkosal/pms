@extends('layouts.master')
@section('title', 'category')
@section('content','Create category')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-6">
        <div class="card card-danger card-outline">
            <div class="card-header">
                <h3 class="card-title">Please input your information</h3>
            </div>
            <div class="card-body">

                @if ($errors->Any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                    @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                <form method="POST" action="{{ route('categories.update', $category->id) }}">
                    @csrf
                    @method('patch')
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter name" name="name"
                            value="{{ $category->name }}">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" placeholder="Enter description"
                            name="description">{{ $category->description }}</textarea>
                    </div>
                    <button type="button" class="btn btn-danger">Cencel</button>
                    <button type="Submit" class="btn btn-success float-right">Save</button>

                </form>

            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endsection
