@if (count($categories)>0)

    @foreach ($categories as $category)
    <tr>
        <td>{{  $category->id }}</td>
        <td>{{ $category->name }}</td>
        <td>{{ $category->description }} </td>
        <td>
            <div class="btn-group">
                <a href="{{ route('categories.show', $category->id) }}" class="btn btn-info">
                    <i class="fa fa-eye"></i>
                </a>
                <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                </a>
                <a class="btn btn-danger btn-delete" data-id='{{ $category->id }}'>
                    <i class="fa fa-trash"></i>
                </a>
            </div>
        </td>
    </tr>
    @endforeach
    
@endif