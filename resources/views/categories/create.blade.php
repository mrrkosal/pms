@extends('layouts.master')
@section('title', 'category')
@section('content','Create category')
@section('big-content','home')

@section('header-content')
<div class="row">
    <div class="col-md-6">
        <div class="card card-danger card-outline">
            <div class="card-header">
                <h3 class="card-title">Please input your information</h3>
            </div>
            <div class="card-body">

                @if ($errors->Any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5><i class="icon fa fa-warning"></i> Alert!</h5>
                    @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                    @endforeach
                </div>
                @endif
                <form method="POST" action="{{ route('categories.store') }}">
                    @csrf

                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Enter name" name="name">
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" rows="3" pl aceholder="Enter description"
                            name="description"></textarea>
                    </div>
                    <button type="button" class="btn btn-danger">Cencel</button>
                    <button type="Submit" class="btn btn-success float-right">Create</button>

                </form>

            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>
@endif



<script>
    $(document).ready(function () {
        console.log("hello");

        let spinner = `
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>`;
        $('#form').submit((event) => {
            console.log("hi");

            event.preventDefault()
            let search = $('#search').val()
            console.log(search)
            $('tbody').html(spinner)
            $.ajax({
                url: '{{ route("categories.index") }}',
                method: 'GET',
                data: {
                    search: search
                },
                success: function (results) {
                    console.log(results);
                    // listCategory(results.data)
                    $('tbody').empty().html(results)
                },
                error: function (errors) {
                    console.log(errors);
                }
            })

        })
        // let listCategory = function () {
        // let content = ``
        // for (category of categories) {
        // content += `
        // <tr>
        // <td>${ category.id }</td>
        // <td>${ category.name }</td>
        // <td>${ category.description } </td>
        // <td>
        // <div class="btn-group">
        // <a href="{{ route('categories.show', $category->id) }}" class="btn btn-info">
        // <i class="fa fa-eye"></i>
        // </a>
        // <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-success">
        // <i class="fa fa-edit"></i>
        // </a>
        // <a class="btn btn-danger btn-delete" data-id='{{ $category->id }}'>
        // <i class="fa fa-trash"></i>
        // </a>
        // </div>
        // </td>
        // </tr>
        // `
        // }
        // $('tbody').empty().html(results.data)
        // }

    })

</script>
@endsection
