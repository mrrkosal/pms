@extends('layouts.master')
@section('title', 'category')
@section('content','Create category')
@section('big-content','home')

@section('header-content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-danger card-outline">
                <div class="card-header">
                    <h3 class="card-title">Please show data your information</h3>
                </div>
                <div class="card-body">
                    <p>{{ $category->name }}</p>
                    <p>{{ $category->description }}</p>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</div>
@endsection
