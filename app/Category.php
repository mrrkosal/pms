<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // Right all which one kor ban
    // protected $gurde=[' '];
    protected $fillable = [
        'name',
        'description'
    ];
}
