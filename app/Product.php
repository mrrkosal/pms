<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    // protected $guarded = [''];
    protected $fillable = [
            'name',
            'code',
            'price',
            'onhand',
            'supplier_name',
            'categories_id',
            'imageurl',
    ];
}
 