<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Category;
use RealRashid\SweetAlert\Facades\Alert;




class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (session()->get('success')) {
            Alert::success('Success!');
        }
         if (request()->has('search')) {
             $search = request()->input('search');
             $categories = Category::where('name','like','%'.$search.'%')
                                    ->orderBy('name','DESC')
                                    ->paginate();
             return view('categories.result')->with('categories', $categories);
         }
         else{
            $categories = Category::orderBy('id', 'DESC')
                                    ->paginate(5);
            return view('categories.lists', compact('categories'));
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validat = $request->validate([
            'name' => 'required|unique:categories|max:150',
            'description' => 'required|max:255',
        ]);
        Category::create($validat);
        return redirect()->route('categories.index')->with('success','You complete successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category:: findOrFail($id);
        return view('categories.showdata', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category:: findOrFail($id);
        return view('categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validat = $request->validate([
            'name' => 'required|max:150',
            'description' => 'max:255',
        ]);
        Category::where('id',$id)->update($validat);
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reguest $reguest)
    {
        Category::where('id',$reguest->id)->delete();
        return redirect()->route('categories.index');
    }
}
