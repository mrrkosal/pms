<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('id','DESC')->paginate(3);
        return view('products.lists', compact('products'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('products.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name'=> '',
            'code'=> '',
            'supplier_name'=>'',
            'category_id'=>'',
            'price'=> '',
            'onhand'=> '',
            'imageurl'=> 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $product =new Product;
        $product->name = $request->name;
        $product->code = $request->code;
        $product->supplier_name = $request->supplier_name;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->onhand = $request->onhand;
        $path = $request->file('imageurl')->store('uploads/img', 'public');
        $product->imageurl = $path;
        $product->save();
            
        // $products ::create($validated);
        return  redirect()->route('products.index');
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product:: findOrFail($id);
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $categories= Category::all();
        $product = Product:: findOrFail($id);
        return view('products.editnew', compact('product','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $validated = $request->validate([
            'name'=> '',
            'code'=> '',
            'supplier_name'=>'',
            'category_id'=>'',
            'price'=> '',
            'onhand'=> '',
            'imageurl'=> 'mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $product =new Product;
        $product->name = $request->name;
        $product->code = $request->code;
        $product->supplier_name = $request->supplier_name;
        $product->category_id = $request->category_id;
        $product->price = $request->price;
        $product->onhand = $request->onhand;
        $path = $request->file('imageurl')->store('uploads/img', 'public');
        $product->imageurl = $path;
        $product->save();
                
        Product::where('id',$id)->update($validated);
        return redirect()->route('products.index');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Product::where('id',$reguest->id)->delete();
        return redirect()->route('product.index');
    }
}
